package accountapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/alfredn/goJwtApi/entities"
	jwt "github.com/dgrijalva/jwt-go"
)

var secretKey = "MySecretKey"

func GenerateToken(response http.ResponseWriter, request *http.Request) {
	var account entities.Account
	err := json.NewDecoder(request.Body).Decode(&account)
	if err != nil {
		respondWithError(response, http.StatusBadRequest, err.Error())
	} else {
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username": account.Username,
			"password": account.Password,
			"exp":      time.Now().Add(time.Hour * 72).Unix(),
		})
		tokenString, err2 := token.SignedString([]byte(secretKey))
		if err2 != nil {
			respondWithError(response, http.StatusBadRequest, err.Error())
		} else {
			respondWithJson(response, http.StatusOK, entities.JWTToken{Token: tokenString})
		}
	}
}

func CheckToken(response http.ResponseWriter, request *http.Request) {
	tokenString := request.Header.Get("key")
	result, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(secretKey), nil
	})
	if err == nil && result.Valid {
		fmt.Println("Valid")
	} else {
		fmt.Println("InValid")
	}
}

func respondWithError(w http.ResponseWriter, code int, msg string) {
	respondWithJson(w, code, map[string]string{"error": msg})
}

func respondWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
