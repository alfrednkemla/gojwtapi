package main

import (
	"fmt"
	"net/http"

	"github.com/alfredn/goJwtApi/apis/demoapi"
	"github.com/alfredn/goJwtApi/middlewares/jwtauth"

	"github.com/alfredn/goJwtApi/apis/accountapi"
	"github.com/gorilla/mux"
)

func main() {
	fmt.Println("Starting the application...")
	router := mux.NewRouter()
	router.HandleFunc("/api/account/generatetoken", accountapi.GenerateToken).Methods("POST")
	router.HandleFunc("/api/account/checktoken", accountapi.CheckToken).Methods("GET")
	router.HandleFunc("/api/demo/demo1", demoapi.Demo1).Methods("GET")
	router.HandleFunc("/api/demo/demo2", demoapi.Demo2).Methods("GET")
	router.Use(jwtauth.JWTAuth)
	err := http.ListenAndServe(":4000", router)
	if err != nil {
		fmt.Println(err)
	}
}
